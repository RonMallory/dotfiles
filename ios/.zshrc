# If you come from bash you might have to change your $PATH.
# export PATH=$HOME/bin:/usr/local/bin:$PATH

# Path to your oh-my-zsh installation.
export ZSH="$HOME/.oh-my-zsh"

# Set name of the theme to load --- if set to "random", it will
# load a random theme each time oh-my-zsh is loaded, in which case,
# to know which specific one was loaded, run: echo $RANDOM_THEME
# See https://github.com/ohmyzsh/ohmyzsh/wiki/Themes
#ZSH_THEME="robbyrussell"
ZSH_THEME="eastwood"
# Set list of themes to pick from when loading at random
# Setting this variable when ZSH_THEME=random will cause zsh to load
# a theme from this variable instead of looking in $ZSH/themes/
# If set to an empty array, this variable will have no effect.
# ZSH_THEME_RANDOM_CANDIDATES=( "robbyrussell" "agnoster" )

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion.
# Case-sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment one of the following lines to change the auto-update behavior
# zstyle ':omz:update' mode disabled  # disable automatic updates
# zstyle ':omz:update' mode auto      # update automatically without asking
# zstyle ':omz:update' mode reminder  # just remind me to update when it's time

# Uncomment the following line to change how often to auto-update (in days).
# zstyle ':omz:update' frequency 13

# Uncomment the following line if pasting URLs and other text is messed up.
# DISABLE_MAGIC_FUNCTIONS="true"

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# You can also set it to another string to have that shown instead of the default red dots.
# e.g. COMPLETION_WAITING_DOTS="%F{yellow}waiting...%f"
# Caution: this setting can cause issues with multiline prompts in zsh < 5.7.1 (see #5765)
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# You can set one of the optional three formats:
# "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# or set a custom format using the strftime function format specifications,
# see 'man strftime' for details.
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load?
# Standard plugins can be found in $ZSH/plugins/
# Custom plugins may be added to $ZSH_CUSTOM/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(git)

source $ZSH/oh-my-zsh.sh

# User configuration

# export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"

# ---------------------
# System Aliases
# ---------------------
# vi vim
alias vi='vim'
# ls color
alias ls='ls --color=auto'
# alias lookup cat bash_alias
alias al='cat ~/.bash_aliases'
# sort by file size
alias lt='ls --human-readable --size -1 -S --classify'
# find command in grep history
alias ghist='history|grep'
# sort by modification time
alias left='ls -t -l'
# count files in directory
alias count='find . -type f | wc -l'
# create python virtual environment
alias ve='python3 -m venv ./venv'
alias va='source ./venv/bin/activate'
# protect files from accidently being removed
alias tcn='mv --force -t ~/.local/share/Trash'
# df human readable
alias df="df -Tha --total"
# du human readable
alias du="du -ach | sort -h"
# free human readable
alias free="free -mt"
# ps human readable
alias ps="ps auxf"
# searchable process table
alias psg="ps aux | grep -v grep | grep -i -e VSZ -e"
# mkdir default
alias mkdir="mkdir -pv"
# wget default
alias wget="wget -c"
# ----------------------
# Vagrant Aliases
# ----------------------
## create a snapshot
alias vspu='vagrant snapshot push'
## restore to snapshot
alias vspo='vagrant snapshot pop'
## start vagrant
alias vu='vagrant up'
## vagrant destroy
alias vd='vagrant destroy --force'
## vagrant up and snapshot push
alias vusp='vagrant up && snapshot push'
# ----------------------
# Git Aliases
# ----------------------
alias ga='git add'
alias gaa='git add --all'
alias gb='git branch'
alias gba='git branch --all'
alias gbd='git branch --delete '
alias gc='git commit'
alias gcm='git commit --message'
alias gco='git checkout'
alias gcob='git checkout -b'
alias gcom='git checkout main'
alias gcos='git checkout staging'
alias gcod='git checkout develop'
alias gd='git diff'
alias gda='git diff HEAD'
alias gi='git init'
alias glg='git log --graph --oneline --decorate --all'
alias gld='git log --pretty=format:"%h %ad %s" --date=short --all'
alias gm='git merge --no-ff'
alias gma='git merge --abort'
alias gmc='git merge --continue'
alias gp='git pull'
alias gpr='git pull --rebase'
alias gr='git rebase'
alias gs='git status'
alias gss='git status --short'
alias gst='git stash'
alias gsta='git stash apply'
alias gstd='git stash drop'
alias gstl='git stash list'
alias gstp='git stash pop'
alias gsts='git stash save'
alias grh1='git reset HEAD~1'
alias gsquash='git reset $(git merge-base main $(git rev-parse --abbrev-ref HEAD))'
# ----------------------
# Ansible Aliases
# ----------------------
alias ap='ansible-playbook'
alias ad='ansible-doc'
alias ag='ansible-galaxy'
alias alint='ansible-lint'
alias ylint='yamllint'
# ----------------------
# Molecule Aliases
# ----------------------
alias mt='molecule test'
alias mtd='molecule --debug test'
alias mc='molecule converge'
alias mver='molecule verify'
# ----------------------
# Terraform Aliases
# ----------------------
# TerraForm MOdule Explained
function tfmoe {
  echo -e "\nOutputs:"
  grep -r "output \".*\"" $1 |awk '{print "\t",$2}' |tr -d '"'
  echo -e "\nVariables:"
  grep -r "variable \".*\"" $1 |awk '{print "\t",$2}' |tr -d '"'
}
#TerraForm MOdule Initialize
function tfmoi {
  touch $1/variables.tf
  touch $1/outputs.tf
  touch $1/versions.tf
  touch $1/main.tf
}
alias tf='terraform'
alias tfv='terraform validate'
alias tfi='terraform init'
alias tfp='terraform plan'
alias tfpb='terraform plan -out build.out'
alias tfpd='terraform plan -destroy -out destroy.out'
alias tfab='terraform apply build.out'
alias tfad='terraform apply destroy.out'
alias tfm='terraform fmt -recursive'


# ----------------------
# kubctl Aliases
# ----------------------
alias k="kubectl"
alias kgn="kubectl get namespaces"


# ----------------------
# Pulumi Aliases
# ----------------------
alias p="pulumi"
alias pu="pulumi up --yes"
alias pd="pulumi destroy --yes"
alias pr="pulumi refresh --yes"
alias pshowurn="pulumi stack --show-urns"
alias pconfig="pulumi config"

# ----------------------
#     Azure
# ---------------------
# BEGIN Ansible Managed Block for azure autocomplete
autoload -U +X bashcompinit && bashcompinit
source /usr/local/etc/bash_completion.d/az
# END Ansible Managed Block for azure autocomplete
